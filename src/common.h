#ifndef COMMON_H_
#define COMMON_H_

#include <assert.h>
#include <ostream>

template<typename T>
class Volume {
private:
	std::vector<T> data;
	unsigned int width, height, depth;
public:
	Volume() : width(0), height(0), depth(0) {}
	Volume(unsigned int w, unsigned int h = 1, unsigned int d = 1) : width(w), height(h), depth(d) {
		data.resize(w*h*d);
	}

	void Resize(unsigned int newW, unsigned int newH = 1, unsigned int newD = 1) {
		data.resize(newW*newH*newD);
		width = newW;
		height = newH;
		depth = newD;
	}

	inline T operator()(unsigned int i = 0, unsigned int j = 0, unsigned int k = 0) const {
		assert(i < width && j < height && k < depth);
		return data[k*width*height + j*width + i];
	}

	inline T& operator()(unsigned int i = 0, unsigned int j = 0, unsigned int k = 0) {
		assert(i < width && j < height && k < depth);
		return data[k*width*height + j*width + i];
	}

	inline Volume<T> operator*(const Volume<T>& input) const {
		auto mult = [&](double a, double b) {
			return a*b;
		};
		return applyBinary(input, mult);
	}

	inline Volume<T>& operator*=(const Volume<T>& input) {
		auto mult = [&](double a, double b) {
			return a*b;
		};
		return applyBinary(input, mult);
	}

	template<typename MapFunction>
	inline Volume<T>& applyBinary(const Volume<T>& input, MapFunction func) {
		assert(checkCompatibility(input));
		for (int k = 0; k < input.Depth(); ++k) {
			for (int j = 0; j < input.Height(); ++j) {
				for (int i = 0; i < input.Width(); ++i) {
					(*this)(i, j, k) = func((*this)(i, j, k), input(i, j, k));
				}
			}
		}
		return (*this);
	}

	template<typename MapFunction>
	inline Volume<T>& applyBinary(const Volume<T>& input, MapFunction func) const {
		assert(checkCompatibility(input));
		Volume<T> output = input;
		for (int k = 0; k < input.Depth(); ++k) {
			for (int j = 0; j < input.Height(); ++j) {
				for (int i = 0; i < input.Width(); ++i) {
					output(i, j, k) = func((*this)(i, j, k), input(i, j, k));
				}
			}
		}

		return output;
	}

	inline bool checkCompatibility(const Volume<double>& input) const {
		if (Width() != input.Width() || Height() != input.Height() || Depth() != input.Depth()) {
			return false;
		}
		return true;
	}
	
	inline void Null() {
		std::fill(data.begin(), data.end(), T(0));
	}

	inline typename std::vector<T>::const_iterator cbegin() const {
		return data.cbegin();
	}

	inline typename std::vector<T>::const_iterator cend() const {
		return data.cend();
	}

	inline typename std::vector<T>::iterator begin() {
		return data.begin();
	}

	inline typename std::vector<T>::iterator end() {
		return data.end();
	}

	inline unsigned int Width() const { return width; }
	inline unsigned int Height() const { return height; }
	inline unsigned int Depth() const { return depth; }

	friend std::ostream& operator<<(std::ostream& os, const Volume<T>& v) {
		unsigned int w = os.width();
		for (int k = 0; k < v.Depth(); ++k) {
			for (int j = 0; j < v.Height(); ++j) {
				for (int i = 0; i < v.Width(); ++i) {
					os.width(w);
					os << v(i, j, k) << " ";
				}
				os << std::endl;
			}
			os << std::endl;
		}
		return os;
	}
};

typedef Volume<double> inputData;
typedef Volume<double> outputData;
typedef unsigned int classesType;

typedef std::vector<inputData> inputVector;
typedef std::vector<outputData> outputVector;
typedef inputVector::const_iterator iV_cit;
typedef outputVector::const_iterator oV_cit;
typedef std::pair<iV_cit, iV_cit> iV_interval;

typedef std::vector<classesType> classesVector;
typedef classesVector::const_iterator cV_cit;
typedef std::pair<cV_cit, cV_cit> cV_interval;

typedef std::vector<std::vector<double> > oneHotd;
#endif // !COMMON_H_
