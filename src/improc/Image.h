#include <stdint.h>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <iterator>
#include <assert.h>
#include <algorithm>

#include <common.h>

struct BMPHeader {
	// File Header
	uint16_t   FileType;
	uint32_t   FileSize;
	uint16_t   Reserved1;
	uint16_t   Reserved2;
	uint32_t   BitmapOffset;
	// Bitmap Header
	uint32_t   BitmapHeaderSize;
	int32_t    Width;
	int32_t    Height;
	uint16_t   Planes;
	uint16_t   BitsPerPixel;
	uint32_t   Compression;
	uint32_t   SizeOfBitmap;
	int32_t    HorzResolution;
	int32_t    VertResolution;
	uint32_t   ColorsUsed;
	uint32_t   ColorsImportant;
};

/**
 * A class for handling input images.
 * 
 * Only BMP images are supported.
 */
class Image {
private:
	Volume<unsigned char> data;
	
	void readBMPHeader(std::ifstream& ifs, BMPHeader& h) {
		ifs.read( (char*)&(h.FileType),         2);
		if ( h.FileType != (uint16_t) 0x4d42 ) throw std::string("Unknown file type!");
		
		ifs.read( (char*)&(h.FileSize),         4);
		ifs.read( (char*)&(h.Reserved1),        2);
		ifs.read( (char*)&(h.Reserved2),        2);
		ifs.read( (char*)&(h.BitmapOffset),     4);
		
		ifs.read( (char*)&(h.BitmapHeaderSize), 4);
		ifs.read( (char*)&(h.Width),            4);
		ifs.read( (char*)&(h.Height),           4);
		ifs.read( (char*)&(h.Planes),           2);
		ifs.read( (char*)&(h.BitsPerPixel),     2);
		ifs.read( (char*)&(h.Compression),      4);
		ifs.read( (char*)&(h.SizeOfBitmap),     4);
		ifs.read( (char*)&(h.HorzResolution),   4);
		ifs.read( (char*)&(h.VertResolution),   4);
		ifs.read( (char*)&(h.ColorsUsed),       4);
		ifs.read( (char*)&(h.ColorsImportant),  4);		
	}
	
	void loadFromFile(const std::string& fname) {
		std::ifstream ifs(fname.data(), std::ios::binary);
		if ( ifs.fail( ) )
			throw std::string( "Failed to open BMP file: " + fname );
		
		BMPHeader header;
		readBMPHeader(ifs, header);
		/*
		std::cout << header.FileType << std::endl;
		std::cout << header.FileSize << std::endl;
		std::cout << header.Width << std::endl;
		std::cout << header.Height << std::endl;
		std::cout << header.BitsPerPixel << std::endl;
		std::cout << header.BitmapOffset << std::endl;
		*/
		ifs.seekg(header.BitmapOffset, std::ios_base::beg);
		
		int width = header.Width;
		int height = header.Height;
		
		if ( header.BitsPerPixel == 24 ) {
			data.Resize(width, height, 3);
			unsigned char r, g, b;
			for (int i = height-1; i >= 0; --i) { // flip image if necessary
				for (int j = 0; j < width; ++j) { 
					// assuming BGR order
					ifs.read((char*)&b, 1); ifs.read((char*)&g, 1); ifs.read((char*)&r, 1);
					data(j, i, 0) = r; data(j, i, 1) = g; data(j, i, 2) = b;
				}
				int pad = (width*3L) % 4L;
				if ( pad ) {
					while ( pad < 4 && !ifs.eof() ) {
						ifs.read((char*)&b, 1);
						pad++;
					}
				}
			}
		} else {
			throw std::string("Unsupported BitsPerPixel value: ") + std::to_string(header.BitsPerPixel);
		}
	}
	
public:		
	Image() {}
	Image(const std::string& fname) {
		loadFromFile(fname);
	}
	Volume<double> createNormVolume() const {
		Volume<double> retVal(data.Width(), data.Height(), data.Depth());
		std::vector<unsigned char> sorted;
		for (int k = 0; k < data.Depth(); ++k) {
			sorted.clear();
			std::copy(data.cbegin() + k*data.Width()*data.Height(), data.cbegin() + (k+1)*data.Width()*data.Height(), std::back_inserter(sorted));
			std::sort(sorted.begin(), sorted.end());
			double medval = 0;
			if (sorted.size() % 2 == 1) {
				medval = sorted[sorted.size() / 2];
			} else {
				medval = (sorted[sorted.size() / 2] + sorted[sorted.size() / 2 + 1]) / 2;
			}

			for	(int j = 0; j < data.Height(); ++j) {
				for (int i = 0; i < data.Width(); ++i) {
					retVal(i, j, k) = (((double)data(i, j, k))-medval) / 255.0;
				}
			}
		}
		return retVal;
	}
};
