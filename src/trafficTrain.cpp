#include <iostream>

#include "improc/Image.h"
#include "cnn/layers.h"
#include "cnn/network.h"
#include "common.h"
#include <chrono>

#include "traffic_networks.h"

int main(int argc, char** argv) {
	if (argc != 3) {
		std::cerr << "Usage: " << argv[0] << " PATH_OF_DATA PATH_OF_CONFIGURATION_FILE" << std::endl;
		return -1;
	}

	inputVector trainingData;
	classesVector annotation;
	readAdasData(argv[1], argv[2], trainingData, annotation);

	unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::cout << "Seed: " << seed << std::endl;
	NeuralNetwork nn(seed);
	
	network_001(nn);

	nn.fit(trainingData, annotation, 12, NETWORK_001_PARAMS);

	nn.writeParametersToFile(NETWORK_001_OUTPUT_NAME);
	
	/*
	network_002(nn);

	nn.fit(trainingData, annotation, 12, NETWORK_002_PARAMS);

	nn.writeParametersToFile(NETWORK_002_OUTPUT_NAME);
	*/

	return 0;
}