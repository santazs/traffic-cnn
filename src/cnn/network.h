#ifndef NETWORK_H_
#define NETWORK_H_

#include <vector>
#include <memory>

#include "layers.h"
#include <common.h>
#include <iostream>

class NeuralNetwork {
private:
	std::vector<Layer*> layers;
	std::vector<std::shared_ptr<Layer> > layer_catcher;
	
	oneHotd last_softmax;

	template<typename layer_type>
	void layer_push(layer_type&& l, std::true_type) {
		layer_catcher.push_back(std::make_shared<typename std::remove_reference<layer_type>::type>(std::forward<layer_type>(l)));
		layers.push_back(layer_catcher.back().get());
	}

	template<typename layer_type>
	void layer_push(layer_type&& l, std::false_type) {
		layers.push_back(&l);
	}

	inline void softmax(const outputVector& output, oneHotd& classes) {
		classes.resize(output.size());		
		int ind = 0;
		for (auto o_it = output.cbegin(); o_it != output.cend(); ++o_it, ++ind) {
			//double maxVal = *std::max_element(o_it->cbegin(), o_it->cend());
			double sumVal = 0.0;
			for (auto i_it = (*o_it).cbegin(); i_it != (*o_it).cend(); ++i_it) {
				sumVal += exp((*i_it));
			}
			if (sumVal < 1e-10) sumVal = 1e-10;
			for (auto i_it = (*o_it).cbegin(); i_it != (*o_it).cend(); ++i_it) {
				classes[ind].push_back(exp((*i_it)) / sumVal);
			}
		}
	}

	inline void oneHotRepresentation(const classesVector& gt_classes, oneHotd& output, int number_of_classes = -1) {
		output.resize(gt_classes.size());
		if (number_of_classes == -1) {
			unsigned int maxVal = 0;
			for (auto i_it = gt_classes.begin(); i_it != gt_classes.end(); ++i_it) {
				if (maxVal < (*i_it)) maxVal = (*i_it);
			}
			number_of_classes = maxVal + 1;
		}
		for (int i = 0; i < output.size(); ++i) {
			output[i].resize(number_of_classes, 0.0);			
			output[i][gt_classes[i]] = 1.0;
		}
	}

	inline classesType predict(const inputData& input) {
		if (layers.size() > 0) {
			layers[0]->forward(input);
			for (int i = 1; i < layers.size(); ++i) {
				layers[i]->forward(layers[i - 1]->getOutput());
			}
			outputData lastOutput = layers.back()->getOutput();
			oneHotd predictions;
			softmax({ lastOutput }, predictions);
			last_softmax.push_back(predictions[0]);

			unsigned int maxInd = 0;
			double maxVal = 0.0;
			for (unsigned int i = 0; i < predictions[0].size(); ++i) {
				if (maxVal < predictions[0][i]) {
					maxVal = predictions[0][i];
					maxInd = i;
				}
			}

			return maxInd;
		}
		return -1;
	}

	inline double error(const inputVector& input, const oneHotd& annotation, bool use_last_softmax = false) {
		double retVal = 0.0;
		oneHotd predictions;
		predictions.reserve(input.size());
		if (!use_last_softmax) {
			if (layers.size() > 0) {
				for (unsigned int i = 0; i < input.size(); ++i) {
					layers[0]->forward(input[i]);
					for (int i = 1; i < layers.size(); ++i) {
						layers[i]->forward(layers[i - 1]->getOutput());
					}
					outputData lastOutput = layers.back()->getOutput();
					oneHotd pred;
					softmax({ lastOutput }, pred);
					predictions.push_back(pred[0]);
				}
			}
		} else {
			predictions = last_softmax;
		}
		for (unsigned int i = 0; i < predictions.size(); ++i) {
			for (unsigned int j = 0; j < predictions[i].size(); ++j) {
				if (predictions[i][j] < 1e-10) predictions[i][j] = 1e-10;
				retVal += annotation[i][j] * std::log(predictions[i][j]);
			}
		}
		return -retVal / input.size();
	}

	std::shared_ptr<std::mt19937> rng;

public:
	NeuralNetwork(unsigned int seed = 5489u) {
		rng = std::make_shared<std::mt19937>(seed);
	}

	void predict(const inputVector& input, classesVector& predictions) {
		predictions.clear();
		predictions.reserve(input.size());
		last_softmax.clear();
		for (unsigned int i = 0; i < input.size(); ++i) {
			predictions.push_back(predict(input[i]));
		}
	}

	template<typename layer_type>
	void push(layer_type&& l) {
		layer_push(std::forward<layer_type>(l), typename std::is_rvalue_reference<decltype(l)>::type());
		layers.back()->assignRandom(rng);
	}

	void testNN(const inputData& input, const classesType& annotation) {
		oneHotd classes_onehot;
		oneHotRepresentation({ annotation }, classes_onehot, 3);
		for (auto it : classes_onehot[0])
			std::cout << it << " ";
		std::cout << std::endl;

		layers[0]->forward(input);
		std::cout << "Layer 0 - Forward Output:" << std::endl;
		std::cout << layers[0]->getOutput();
		for (int i = 1; i < layers.size(); ++i) {
			layers[i]->forward(layers[i - 1]->getOutput());
			std::cout << "Layer " << i << " - Forward Output:" << std::endl;
			std::cout << layers[i]->getOutput();
		}
		outputData lastOutput = layers.back()->getOutput();

		oneHotd predictions;
		softmax({ lastOutput }, predictions);
		std::cout << "Softmax Layer output:" << std::endl;
		for (auto it : predictions[0])
			std::cout << it << " ";
		std::cout << std::endl << std::endl;

		for (auto img_ind = 0; img_ind < 1; ++img_ind) {
			int class_ind = 0;
			for (auto i_it = lastOutput.begin(); i_it != lastOutput.end(); ++i_it, ++class_ind) {
				(*i_it) = (predictions[img_ind][class_ind] - classes_onehot[img_ind][class_ind]);
			}
		}

		std::cout << "Cross Entropy Gradient" << std::endl;
		std::cout << lastOutput;

		layers.back()->backward(lastOutput);
		std::cout << "Layer " << layers.size()-1 << " - Backward Output:" << std::endl;
		std::cout << layers.back()->getBackwardOutput();
		for (int i = layers.size() - 2; i >= 0; --i) {
			layers[i]->backward(layers[i + 1]->getBackwardOutput());
			std::cout << "Layer " << i << " - Backward Output:" << std::endl;
			std::cout << layers[i]->getBackwardOutput();
		}

		for (auto it = layers.begin(); it != layers.end(); ++it) {
			if ((*it)->hasParameters()) (*it)->updateParametersByGradient(0.1);
		}
	}

	void fit(const inputVector& input, const classesVector& input_classes, int number_of_classes = -1,
		double learning_rate = 0.1, unsigned long max_iter = 15L, unsigned long batch_size = 64L) 
	{
		unsigned long N_input = input.size();
		unsigned long N_batches = N_input / batch_size + ((N_input % batch_size > 0)? 1 : 0);
		oneHotd classes_onehot;
		oneHotRepresentation(input_classes, classes_onehot, number_of_classes);

		unsigned long iter = 0;
		while (iter < max_iter) {
			++iter;
			for (unsigned long b = 0; b < N_batches; ++b) {
				inputVector batch;
				std::copy(input.cbegin() + b*batch_size, input.cbegin() + std::min((b + 1)*batch_size, N_input), std::back_inserter(batch));

				oneHotd batch_classes_onehot;
				std::copy(classes_onehot.cbegin() + b*batch_size, classes_onehot.cbegin() + std::min((b + 1)*batch_size, N_input), std::back_inserter(batch_classes_onehot));

				if (layers.size() > 0) {
					layers[0]->batchForward(batch);
					for (int i = 1; i < layers.size(); ++i) {
						layers[i]->batchForward(layers[i - 1]->getBatchOutput());
					}
					outputVector lastOutput = layers.back()->getBatchOutput();

					// default top layer: softmax layer with cross entropy error
					oneHotd predictions;
					softmax(lastOutput, predictions);
					
					for (auto img_ind = 0; img_ind < lastOutput.size(); ++img_ind) { 
						int class_ind = 0;
						for (auto i_it = lastOutput[img_ind].begin(); i_it != lastOutput[img_ind].end(); ++i_it, ++class_ind) {
							(*i_it) = (predictions[img_ind][class_ind]-batch_classes_onehot[img_ind][class_ind]);
						}
					}

					layers.back()->batchBackward(lastOutput);
					for (int i = layers.size() - 2; i >= 0; --i) {
						layers[i]->batchBackward(layers[i + 1]->getBatchBackwardOutput());
					}

					for (auto it = layers.begin(); it != layers.end(); ++it) {
						if ((*it)->hasParameters()) (*it)->updateParametersByGradient(learning_rate);
					}
				}
			}

			classesVector output_pred;
			predict(input, output_pred);

			unsigned int good = 0;
			unsigned int bad = 0;
			for (unsigned int i = 0; i < input_classes.size(); ++i) {
				if (input_classes[i] == output_pred[i]) good++;
				else bad++;
			}

			std::cout << "Epoch " << iter << std::endl;
			std::cout << "E = " << error(input, classes_onehot, true) << std::endl;
			std::cout << "Good: " << good << " " << (double)good / N_input * 100 << "%,  Bad: " << bad << " " << (double)bad / N_input * 100 << "%" << std::endl;
		}
	}

	void test(const inputVector& input, const classesVector& input_classes, unsigned int number_of_classes, classesVector& preditions) {
		oneHotd classes_onehot;
		oneHotRepresentation(input_classes, classes_onehot, number_of_classes);
		unsigned long N_input = input.size();		
		predict(input, preditions);
		unsigned int good = 0;
		unsigned int bad = 0;
		for (unsigned long i = 0; i < input_classes.size(); ++i) {
			if (input_classes[i] == preditions[i]) good++;
			else bad++;
		}
		std::cout << "E = " << error(input, classes_onehot, true) << std::endl;
		std::cout << "Good: " << good << " " << (double)good / N_input * 100 << "%,  Bad: " << bad << " " << (double)bad / N_input * 100 << "%" << std::endl;

		std::vector<std::vector<unsigned long> > confusion;
		confusion.resize(number_of_classes);
		for (unsigned int i = 0; i < number_of_classes; ++i) confusion[i].resize(number_of_classes, 0);

		for (unsigned long i = 0; i < input_classes.size(); ++i) {
			confusion[input_classes[i]][preditions[i]]++;
		}

		for (unsigned long i = 0; i < number_of_classes; ++i) {
			for (unsigned long j = 0; j < number_of_classes; ++j) {
				std::cout << confusion[i][j] << ((j != number_of_classes - 1) ? " " : "");
			}
			std::cout << std::endl;
		}
	}

	void writeParametersToFile(const std::string& str) {
		std::ofstream ofs(str.data());
		for (auto it = layers.begin(); it != layers.end(); ++it) {
			(*it)->writeToStream(ofs);
		}
	}

	void readParametersFromFile(const std::string& str) {
		std::ifstream ifs(str.data());
		for (auto it = layers.begin(); it != layers.end(); ++it) {
			(*it)->readFromStream(ifs);
		}
	}
};


#endif
