#ifndef LAYERS_H_
#define LAYERS_H_

#include <vector>
#include <algorithm>
#include <numeric>
#include <random>
#include <memory>

#include <common.h>


class Layer {
protected:
	inputData lastInput;
	outputData output;
	outputData backOutput;	

	inputVector batch_lastInput;
	outputVector batch_output;
	outputVector batch_backOutput;

	virtual void batchHandleParametersBegin() {}
	virtual void batchHandleParametersIter() {}
	virtual void batchHandleParametersEnd(unsigned long size_of_batch) {}

	std::shared_ptr<std::mt19937> rndev;

public:
	Layer() {
		rndev = std::make_shared<std::mt19937>();
	}

	virtual ~Layer() {}

	virtual void forward(const inputData&) = 0;
	virtual void backward(const inputData&) = 0;

	void assignRandom(std::shared_ptr<std::mt19937>& rng) {
		rndev.reset();
		rndev = rng;
	}

	void batchForward(const inputVector& input) {
		batch_lastInput = input;
		batch_output.resize(0);
		if (batch_output.capacity() < input.size()) batch_output.reserve(input.size());
		for (auto b_it = input.cbegin(); b_it != input.cend(); ++b_it) {
			forward((*b_it));
			batch_output.push_back(output);
		}
	}

	void batchBackward(const inputVector& outputGrad) {
		batch_backOutput.resize(0);
		if (batch_backOutput.capacity() < outputGrad.size()) batch_backOutput.reserve(outputGrad.size());
		if (hasParameters()) {
			batchHandleParametersBegin();
		}
		auto b_inIt = batch_lastInput.cbegin();
		for (auto b_it = outputGrad.cbegin(); b_it != outputGrad.cend(); ++b_it, ++b_inIt) {
			lastInput = (*b_inIt);
			backward((*b_it));
			batch_backOutput.push_back(backOutput);

			if (hasParameters()) {
				batchHandleParametersIter();
			}
		}
		if (hasParameters()) {
			batchHandleParametersEnd(outputGrad.size());
		}
	}
	
	virtual bool hasParameters() { return false; }

	virtual void updateParametersByGradient(double) {}

	inline outputData& getOutput() {
		return output;
	}
	inline outputData getOutput() const {
		return output;
	}

	inline outputVector& getBatchOutput() {
		return batch_output;
	}
	
	inline outputVector getBatchOutput() const {
		return batch_output;
	}

	inline outputData& getBackwardOutput() {
		return backOutput;
	}
	inline outputData getBackwardOutput() const {
		return backOutput;
	}

	inline outputVector& getBatchBackwardOutput() {
		return batch_backOutput;
	}
	inline outputVector getBatchBackwardOutput() const {
		return batch_backOutput;
	}

	virtual void readFromStream(std::istream& is) {
	}

	virtual void writeToStream(std::ostream& os) {
	}
};

class Convolutional : public Layer {
private:
	unsigned int in_width;
	unsigned int in_height;
	unsigned int in_depth;
	unsigned int kernel_size;
	unsigned int output_depth;
	unsigned int stride_i;
	unsigned int stride_j;
	unsigned int zero_padding;

	std::vector<Volume<double> > W;
	std::vector<Volume<double> > dW;
	std::vector<double> b;
	std::vector<double> db;

	std::vector<Volume<double> > batch_dW;
	std::vector<double > batch_db;

	double weight_decay;

	inline double conv2(const inputData& in, unsigned int i, unsigned int j, unsigned int k, unsigned int out) const {
		double retVal = b[k];
		int ksDiv2 = kernel_size / 2;
		for (int ii = -ksDiv2; ii <= ksDiv2; ++ii) {
			for (int jj = -ksDiv2; jj <= ksDiv2; ++jj) {
				retVal += W[out](ii + ksDiv2, jj + ksDiv2, k) * in(i+ii, j+jj, k);
			}
		}
		return retVal;
	}

	virtual void batchHandleParametersBegin() {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			batch_dW[outerInd].Null();
			batch_db[outerInd] = 0.0;
		}
	}

	virtual void batchHandleParametersIter() {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			for (auto d_it = dW[outerInd].begin(), b_it = batch_dW[outerInd].begin(); d_it != dW[outerInd].end(); ++d_it, ++b_it) {
				(*b_it) += (*d_it);
			}

			batch_db[outerInd] += db[outerInd];
		}
	}

	virtual void batchHandleParametersEnd(unsigned long size_of_batch) {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			for (auto dW_it = dW[outerInd].begin(), b_it = batch_dW[outerInd].begin(), W_it = W[outerInd].begin(); 
				dW_it != dW[outerInd].end(); ++dW_it, ++W_it, ++b_it) 
			{
				(*dW_it) = (*b_it) / size_of_batch - (*W_it)*weight_decay;
			}

			db[outerInd] = batch_db[outerInd] / size_of_batch;;
		}
	}

public:
	Convolutional(unsigned int in_width, unsigned int in_height, unsigned int in_depth, 
		unsigned int kernel_size, unsigned int output_depth, double weight_decay = 0.01,
		unsigned int stride_i = 1, unsigned int stride_j = 1, unsigned int zero_padding = 0) 
	{
		this->in_width = in_width;
		this->in_height = in_height;
		this->in_depth = in_depth;
		this->kernel_size = kernel_size;
		this->output_depth = output_depth;
		this->stride_i = stride_i;
		this->stride_j = stride_j;
		this->zero_padding = zero_padding;
		this->weight_decay = weight_decay;

		std::normal_distribution<> d(0, 0.1);

		W.resize(output_depth);		
		dW.resize(output_depth);
		batch_dW.resize(output_depth);		
		for (unsigned int i = 0; i < output_depth; ++i) {
			W[i].Resize(kernel_size, kernel_size, in_depth);
			dW[i].Resize(kernel_size, kernel_size, in_depth);
			batch_dW[i].Resize(kernel_size, kernel_size, in_depth);
			for (auto W_it = W[i].begin(); W_it != W[i].end(); ++W_it) {
				(*W_it) = d(*rndev.get());
			}
		}
		b.resize(output_depth);
		db.resize(output_depth);
		batch_db.resize(output_depth);

		output.Resize((in_width - kernel_size + 2 * zero_padding) / stride_i + 1, 
			(in_height - kernel_size + 2 * zero_padding) / stride_j + 1, output_depth);
		backOutput.Resize(in_width, in_height, in_depth);
	}

	virtual void forward(const inputData& input) {
		assert(input.Width() == in_width && input.Height() == in_height && input.Depth() == in_depth);
		lastInput = input;
		output.Null();
		int ksDiv2 = kernel_size / 2;
		for (int out = 0; out < output_depth; ++out) {			
			for (int k = 0; k < input.Depth(); ++k) {
				for (int j = ksDiv2, jj = 0; j < input.Height() - ksDiv2; j += stride_j, ++jj) {
					for (int i = ksDiv2, ii = 0; i < input.Width() - ksDiv2; i += stride_i, ++ii) {
						output(ii, jj, out) += conv2(input, i, j, k, out);
					}
				}
			}
		}
	}

	virtual void backward(const inputData& outputGrad) {
		backOutput.Null();
		for (int k = 0; k < db.size(); ++k) {
			unsigned int start_k = k * outputGrad.Width() * outputGrad.Height();
			unsigned int end_k = (k + 1) * outputGrad.Width() * outputGrad.Height();
			db[k] = std::accumulate(outputGrad.cbegin() + start_k, outputGrad.cbegin() + end_k, 0.0);

			dW[k].Null();			
			int ksDiv2 = kernel_size / 2;
			for (unsigned int j = 0; j < outputGrad.Height(); ++j) {
				for (unsigned int i = 0; i < outputGrad.Width(); ++i) {
										
					for (unsigned int kk = 0; kk < in_depth; ++kk) {
						for (int ii = -ksDiv2; ii <= ksDiv2; ++ii) {
							for (int jj = -ksDiv2; jj <= ksDiv2; ++jj) {
								dW[k](ii + ksDiv2, jj + ksDiv2, kk) += lastInput(i + ii + ksDiv2, j + jj + ksDiv2, kk) * outputGrad(i, j, k);
							}
						}
					}
				}
			}

			double val = 0.0;
			for (int outer_j = 0; outer_j < in_height; ++outer_j) {
				for (int outer_i = 0; outer_i < in_width; ++outer_i) {
					int i = outer_i - ksDiv2;
					int j = outer_j - ksDiv2;
					for (int kk = 0; kk < in_depth; ++kk) {
						for (int ii = -ksDiv2; ii <= ksDiv2; ++ii) {
							for (int jj = -ksDiv2; jj <= ksDiv2; ++jj) {
								if (i+ii >= 0 && i+ii < outputGrad.Width() && j+jj >= 0 && j+jj < outputGrad.Height()) {
									val = outputGrad(i + ii, j + jj, k);
								} else {
									val = 0.0;
								}
								backOutput(outer_i, outer_j, kk) += W[k](ksDiv2 - ii, ksDiv2 - jj, kk) * val;
							}
						}
					}
				}
			}
		}
	}

	void updateParametersByGradient(double learning_rate = 0.1) {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			for (auto it = W[outerInd].begin(), d_it = dW[outerInd].begin(); it != W[outerInd].end(); ++it, ++d_it) {
				(*it) -= learning_rate * (*d_it);
			}

			b[outerInd] -= learning_rate * db[outerInd];
		}
	}

	virtual bool hasParameters() { return true; }
	
	friend std::ostream& operator<<(std::ostream& os, const Convolutional& c) {
		os << c.output_depth << " " << c.kernel_size << " " << c.in_depth << std::endl;
		for (auto it : c.W) {
			os << it;
		}

		for (auto it : c.b) {
			os << it << std::endl;
		}

		return os;
	}

	virtual void readFromStream(std::istream& is) {
		for (unsigned int k = 0; k < output_depth; ++k) {
			for (auto it = W[k].begin(); it != W[k].end(); ++it) {
				is >> (*it);
			}
		}
		for (unsigned int k = 0; k < output_depth; ++k) {			
			is >> b[k];
		}
	}

	virtual void writeToStream(std::ostream& os) {
		for (unsigned int k = 0; k < output_depth; ++k) {
			for (auto it = W[k].begin(); it != W[k].end(); ++it) {
				os << (*it) << " ";
			}
		}
		for (unsigned int k = 0; k < output_depth; ++k) {
			os << b[k] << " ";
		}
	}

};

class MaxPooling : public Layer {
private:
	unsigned int in_width;
	unsigned int in_height;
	unsigned int in_depth;
	unsigned int kernel_size;
	unsigned int stride_i;
	unsigned int stride_j;
	unsigned int output_depth;

	Volume<std::pair<unsigned int, unsigned int> > switches;

	inline std::pair<std::pair<unsigned int, unsigned int>, double> max_from_nh(const Volume<double>& in, unsigned int i, unsigned int j, unsigned int k) {
		std::pair<unsigned int, unsigned int> pos;
		double maxval = in(i, j, k);
		for (int ii = i; ii < i + kernel_size; ++ii) {
			for (int jj = j; jj < j + kernel_size; ++jj) {
				if (maxval < in(ii, jj, k)) {
					maxval = in(ii, jj, k);
					pos.first = ii;
					pos.second = jj;
				}
			}
		}
		return {pos, maxval};
	}


public:
	MaxPooling(unsigned int in_width, unsigned int in_height, unsigned int in_depth,
		unsigned int kernel_size = 2, unsigned int stride_i = 2, unsigned int stride_j = 2)
	{
		this->in_width = in_width;
		this->in_height = in_height;
		this->in_depth = in_depth;
		this->kernel_size = kernel_size;
		this->output_depth = in_depth;
		this->stride_i = stride_i;
		this->stride_j = stride_j;

		output.Resize((in_width - kernel_size) / stride_i + 1,
			(in_height - kernel_size) / stride_j + 1, output_depth);
		backOutput.Resize(in_width, in_height, in_depth);
		switches.Resize((in_width - kernel_size) / stride_i + 1,
			(in_height - kernel_size) / stride_j + 1, output_depth);
	}

	virtual void forward(const inputData& input) {
		assert(input.Width() == in_width && input.Height() == in_height && input.Depth() == in_depth);
		lastInput = input;
		for (int out = 0; out < output_depth; ++out) {
			for (int k = 0; k < input.Depth(); ++k) {
				for (int j = 0, jj = 0; j < input.Height() - kernel_size; j += stride_j, ++jj) {
					for (int i = 0, ii = 0; i < input.Width() - kernel_size; i += stride_i, ++ii) {
						auto max_with_pos = max_from_nh(input, i, j, k);
						output(ii, jj, out) = max_with_pos.second;
						switches(ii, jj, out) = max_with_pos.first;
					}
				}
			}
		}
	}

	virtual void backward(const inputData& outputGrad) {
		backOutput.Null();
		for (unsigned int k = 0; k < outputGrad.Depth(); ++k) {
			for (unsigned int j = 0; j < outputGrad.Height(); ++j) {
				for (unsigned int i = 0; i < outputGrad.Width(); ++i) {
					unsigned int ii = switches(i, j, k).first;
					unsigned int jj = switches(i, j, k).second;
					backOutput(ii, jj, k) = outputGrad(i, j, k);
				}
			}
		}
	}
};


template<typename activation_function>
class Activation : public Layer {
private:
	activation_function func;

public:
	Activation() {}

	virtual void forward(const inputData& input) {
		lastInput = input;
		output = func(input);
	}

	virtual void backward(const inputData& outputGrad) {
		backOutput = outputGrad;
		backOutput *= func[lastInput];
	}
};

class ActivationFunctionBase {
protected:
	Volume<double> output;

	inline void checkOutput(const Volume<double>& input) {
		if (output.Width() != input.Width() || output.Height() != input.Height() || output.Depth() != input.Depth()) {
			output = input;
		}
	}
public:
	// Value
	virtual Volume<double>& operator()(const Volume<double>&) = 0;

	// Derivative
	virtual Volume<double>& operator[](const Volume<double>&) = 0;
};

class ReLu : public ActivationFunctionBase {
private:
	inline double apply(double d) {
		return std::max(0.0, d);
	}
	inline double apply_d(double d) {
		return (d > -1e-6)? 1.0 : 0.0;
	}
public:
	virtual Volume<double>& operator()(const Volume<double>& input) {
		checkOutput(input);
		for (int k = 0; k < output.Depth(); ++k) {
			for (int j = 0; j < output.Height(); ++j) {
				for (int i = 0; i < output.Width(); ++i) {
					output(i, j, k) = apply(input(i, j, k));
				}
			}
		}

		return output;
	}

	virtual Volume<double>& operator[](const Volume<double>& input) {
		checkOutput(input);
		for (int k = 0; k < output.Depth(); ++k) {
			for (int j = 0; j < output.Height(); ++j) {
				for (int i = 0; i < output.Width(); ++i) {
					output(i, j, k) = apply_d(input(i, j, k));
				}
			}
		}
		return output;
	}
};

class FullyConnected : public Layer {
private:
	unsigned int in_width;
	unsigned int in_height;
	unsigned int in_depth;
	unsigned int output_depth;
	double weight_decay;

	std::vector<std::vector<double> > W;
	std::vector<std::vector<double> > dW;
	std::vector<double> b;
	std::vector<double> db;

	std::vector<std::vector<double> > batch_dW;
	std::vector<double > batch_db;

	virtual void batchHandleParametersBegin() {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			std::fill(batch_dW[outerInd].begin(), batch_dW[outerInd].end(), 0.0);
		}
		std::fill(batch_db.begin(), batch_db.end(), 0.0);
	}

	virtual void batchHandleParametersIter() {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			for (auto d_it = dW[outerInd].begin(), b_it = batch_dW[outerInd].begin(); d_it != dW[outerInd].end(); ++d_it, ++b_it) {
				(*b_it) += (*d_it);
			}

			batch_db[outerInd] += db[outerInd];
		}
	}

	virtual void batchHandleParametersEnd(unsigned long size_of_batch) {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			for (auto dW_it = dW[outerInd].begin(), b_it = batch_dW[outerInd].begin(), W_it = W[outerInd].begin();
				dW_it != dW[outerInd].end(); ++dW_it, ++b_it) 
			{
				(*dW_it) = (*b_it) / size_of_batch - (*W_it)*weight_decay;
			}

			db[outerInd] = batch_db[outerInd] / size_of_batch;;
		}
	}

public:
	FullyConnected(unsigned int in_width, unsigned int in_height, unsigned int in_depth, unsigned int output_depth, double weight_decay = 0.01) {
		this->in_width = in_width;
		this->in_height = in_height;
		this->in_depth = in_depth;
		this->output_depth = output_depth;
		this->weight_decay = weight_decay;

		std::normal_distribution<> d(0, 0.1);

		W.resize(output_depth);
		for (auto it = W.begin(); it != W.end(); ++it) {
			it->resize(in_width*in_height*in_depth);
			for (auto W_it = it->begin(); W_it != it->end(); ++W_it) {
				(*W_it) = d(*rndev.get());
			}
		}

		dW.resize(output_depth);
		batch_dW.resize(output_depth);
		for (auto it = dW.begin(), b_it = batch_dW.begin(); it != dW.end(); ++it, ++b_it) {
			it->resize(in_width*in_height*in_depth);
			b_it->resize(in_width*in_height*in_depth);
		}
		b.resize(output_depth);
		db.resize(output_depth);
		batch_db.resize(output_depth);

		output.Resize(1, 1, output_depth);
		backOutput.Resize(in_width, in_height, in_depth);
	}

	virtual void forward(const inputData& input) {
		assert(input.Width() == in_width && input.Height() == in_height && input.Depth() == in_depth);
		lastInput = input;
		for (unsigned int out = 0; out < output_depth; ++out) {			
			double val = b[out];
			auto i_it = input.cbegin();
			for (auto W_it = W[out].begin(); W_it != W[out].end(); ++W_it, ++i_it) {
				val += (*W_it) * (*i_it);
			}
			output(0, 0, out) = val;
		}
	}

	virtual void backward(const inputData& outputGrad) {
		int ind = 0;
		for (auto b_it = backOutput.begin(); b_it != backOutput.end(); ++b_it, ++ind) {
			(*b_it) = 0;
			auto og_it = outputGrad.cbegin();
			for (int i = 0; i < W.size(); ++i, ++og_it) {
				(*b_it) += (*og_it) * W[i][ind];
			}
		}
		ind = 0;
		for (auto it = outputGrad.cbegin(); it != outputGrad.cend(); ++it, ++ind) {
			db[ind] = (*it);

			int dW_ind = 0;
			for (auto inp_it = lastInput.cbegin(); inp_it != lastInput.cend(); ++inp_it, ++dW_ind) {
				dW[ind][dW_ind] = (*it) * (*inp_it);
			}
		}
	}

	void updateParametersByGradient(double learning_rate = 0.1) {
		for (unsigned int outerInd = 0; outerInd < W.size(); ++outerInd) {
			for (auto it = W[outerInd].begin(), d_it = dW[outerInd].begin(); it != W[outerInd].end(); ++it, ++d_it) {
				(*it) -= learning_rate * (*d_it);
			}

			b[outerInd] -= learning_rate * db[outerInd];
		}
	}

	virtual bool hasParameters() { return true; }

	friend std::ostream& operator<<(std::ostream& os, const FullyConnected& fc) {
		os << fc.output_depth << " " << fc.in_width*fc.in_height*fc.in_depth << std::endl;
		for (auto it : fc.W) {
			for (auto it2 : it) {
				os << it2 << " ";
			}
			os << std::endl;
		}
		os << std::endl;

		for (auto it : fc.b) {
			os << it << std::endl;
		}

		return os;
	}

	virtual void readFromStream(std::istream& is) {
		for (unsigned int k = 0; k < output_depth; ++k) {
			for (auto it = W[k].begin(); it != W[k].end(); ++it) {
				is >> (*it);
			}
		}
		for (unsigned int k = 0; k < output_depth; ++k) {
			is >> b[k];
		}
	}

	virtual void writeToStream(std::ostream& os) {
		for (unsigned int k = 0; k < output_depth; ++k) {
			for (auto it = W[k].begin(); it != W[k].end(); ++it) {
				os << (*it) << " ";
			}
		}
		for (unsigned int k = 0; k < output_depth; ++k) {
			os << b[k] << " ";
		}
	}
};

#endif // !LAYERS_H_