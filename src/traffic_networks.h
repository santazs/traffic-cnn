#ifndef TRAFFIC_NETWORKS_H_
#define TRAFFIC_NETWORKS_H_

#define NETWORK_001_PARAMS 0.1, 50, 64
#define NETWORK_001_OUTPUT_NAME "network_001_model.txt"

void network_001(NeuralNetwork& nn) {
	nn.push(Convolutional(52, 52, 3, 5, 12, 0.0));
	nn.push(Activation<ReLu>());
	nn.push(MaxPooling(48, 48, 12));

	nn.push(Convolutional(24, 24, 12, 5, 18, 0.0));
	nn.push(Activation<ReLu>());

	nn.push(FullyConnected(20, 20, 18, 12, 0.0));
}

#define NETWORK_002_PARAMS 0.5, 50, 64
#define NETWORK_002_OUTPUT_NAME "network_002_model.txt"

void network_002(NeuralNetwork& nn) {
	nn.push(Convolutional(52, 52,  3, 5, 10, 0.0));
	nn.push(Activation<ReLu>());
	nn.push(Convolutional(48, 48, 10, 5, 10, 0.0));
	nn.push(Activation<ReLu>());
	nn.push(MaxPooling(44, 44, 10));
	
	nn.push(Convolutional(22, 22, 10, 5, 10, 0.0));
	nn.push(Activation<ReLu>());
	nn.push(Convolutional(18, 18, 10, 5, 10, 0.0));
	nn.push(Activation<ReLu>());
	nn.push(MaxPooling(14, 14, 10));
	
	nn.push(FullyConnected(7, 7, 10, 12, 0.0));
}


void readAdasData(const std::string& dataPath, const std::string& configName, inputVector& trainingData, classesVector& annotation) {
	std::ifstream ifs(configName.data());
	std::string line;
	while (std::getline(ifs, line)) {
		std::size_t uscore = line.find_first_of("_");
		classesType c = std::stol(line.substr(0, uscore));
		Image img(dataPath + "/" + std::to_string(c) + "/" + line);

		trainingData.push_back(img.createNormVolume());
		annotation.push_back(c - 1);
	}
}

#endif // !TRAFFIC_NETWORKS_H_
