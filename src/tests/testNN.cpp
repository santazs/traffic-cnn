#include <random>
#include "common.h"
#include "cnn/layers.h"
#include "cnn/network.h"
#include <iostream>
#include <iomanip>      // std::setw

int main() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<> d(0, 2);
	
	/*
	std::cout << std::fixed << std::setprecision(4);
	std::cout << "Input:" << std::endl;
	std::cout << std::setw(8) << vIn;	
	*/
	
	NeuralNetwork nn;
	nn.push(Convolutional(5, 5, 3, 3, 3));
	nn.push(Activation<ReLu>());
	nn.push(FullyConnected(3, 3, 3, 3));
	/*
	// On example test
	Volume<double> vIn(5, 5, 3);
	for (auto it = vIn.begin(); it != vIn.end(); ++it) {
		(*it) = d(gen);
	}

	nn.test(vIn, 1);
	nn.test(vIn, 1);
	nn.test(vIn, 1);
	nn.test(vIn, 1);
	nn.test(vIn, 1);
	nn.test(vIn, 1);
	*/
	
	// Batch test
	Volume<double> v1(5, 5, 3), v2(5, 5, 3), v3(5, 5, 3);
	for (auto it1 = v1.begin(), it2 = v2.begin(), it3 = v3.begin(); it1 != v1.end(); ++it1, ++it2, ++it3) {
		(*it1) = d(gen);
		(*it2) = d(gen);
		(*it3) = d(gen);
	}
	nn.fit({v1, v2, v3}, {0, 0, 1}, 3, 0.1);
}
