#include <random>
#include "common.h"
#include "cnn/layers.h"
#include <iostream>
#include <iomanip>      // std::setw

int main() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<> d(0, 2);
	
	Volume<double> vIn(5, 5, 1);
	for (auto it = vIn.begin(); it != vIn.end(); ++it) {
		(*it) = d(gen);
	}
	
	std::cout << std::fixed << std::setprecision(4);
	std::cout << "Input:" << std::endl;
	std::cout << std::setw(8) << vIn;	
	
	MaxPooling c(5, 5, 1);
	
	std::cout << "has parameters: " << c.hasParameters() << std::endl;
	
	
	c.forward(vIn);
	
	std::cout << std::endl;
	std::cout << "Output:" << std::endl;
	std::cout << c.getOutput();
	
	Volume<double> gIn(2, 2, 1);
	std::fill(gIn.begin(), gIn.end(), 1.0);
	c.backward(gIn);
	
	std::cout << std::endl;
	std::cout << "Backward:" << std::endl;
	std::cout << c.getBackwardOutput();
}
