#include <iostream>

#include "improc/Image.h"
#include "cnn/layers.h"
#include "cnn/network.h"
#include "common.h"
#include <chrono>

#include "traffic_networks.h"

int main(int argc, char** argv) {
	if (argc != 4) {
		std::cerr << "Usage: " << argv[0] << " PATH_OF_DATA PATH_OF_CONFIGURATION_FILE PATH_OF_MODEL_PARAMETERS" << std::endl;
		return -1;
	}

	inputVector testData;
	classesVector annotation;
	readAdasData(argv[1], argv[2], testData, annotation);

	unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::cout << "Seed: " << seed << std::endl;
	NeuralNetwork nn(seed);

	network_001(nn);	
	nn.readParametersFromFile(argv[3]);

	classesVector prediction;
	nn.test(testData, annotation, 12, prediction);

	return 0;
}