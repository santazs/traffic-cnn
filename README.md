# traffic-cnn

## Build
The program has a CMake interface to generate the platform dependent makefiles or VS Solutions. The only dependency is a C++11 compatible compiler.

## Usage
### Train
```bash
trafficTrain PATH_OF_DATA PATH_OF_TRAINING_LIST
```

Herein, ```PATH_OF_DATA``` gives the dataset directory (eg. ...\train-52x52) and the ```PATH_OF_TRAINING_LIST``` is a simple text file containing the names of the training samples. The program will generate a model file into the current working directory with name ```network_001_model.txt```.


### Test
```bash
trafficTest PATH_OF_DATA PATH_OF_TEST_LIST PATH_OF_MODEL
```

Similarly to the training case the ```PATH_OF_DATA``` and ```PATH_OF_TEST_LIST``` contains the path of the dataset and the list of test samples, respectively, while the ```PATH_OF_MODEL``` is the output of the training phase.
